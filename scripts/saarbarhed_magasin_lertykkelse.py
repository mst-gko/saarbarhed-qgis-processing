# -*- coding: utf-8 -*-
__author__ = "Jakob Lanstorp <jakla@mst.dk>"
__version__ = "0.1.0"
__license__ = "GNUv3"

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   Jakob Lanstorp, Miljøstyrelsen 2022                                   *
*                                                                         *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QColor
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterField,
                       QgsProcessingUtils,
                       QgsVectorLayer,
                       QgsWkbTypes,
                       QgsSymbol,
                       QgsRendererRange,
                       QgsGraduatedSymbolRenderer)
from qgis import processing


class SaarbarhedProcessingAlgorithm(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    dest_id = None  # Save a reference to the output layer id

    INPUT_MAGASIN_POLYGON = 'INPUT_MAGASIN_POLYGON'
    INPUT_LERTYKKELSE_POINT = 'INPUT_LERTYKKELSE_POINT'
    INPUT_MAGASIN_ID_COLUMN = 'INPUT_MAGASIN_ID_COLUMN'
    INPUT_LERTYKKELSE_ID_COLUMN = 'INPUT_LERTYKKELSE_ID_COLUMN'
    OUTPUT = 'OUTPUT'

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return SaarbarhedProcessingAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'saarbarhed_magasin_lertykkelse'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Sårbarhedsudtræk')

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr('MST')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'saarbarhedsscripts'

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        hlpstr = """
        Udtræk <b>lertykkelsespunkter</b> så de ligger indenfor deres respektive drikkevandsmagasiner
        
        Styles med lertykkelsessårbarhedsgradient

        <h4>Lille sårbarhed >15 m lertykkelse</h4>
        <h4>Nogen sårbarhed 5-15 m lertykkelse</h4>
        <h4>Stor sårbarhed &lt;5 m lertykkelse</h4>
        """
        return self.tr(hlpstr)

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_MAGASIN_POLYGON,
                self.tr('Magasin polygonlag'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_LERTYKKELSE_POINT,
                self.tr('Lertykkelses punktlag'),
                [QgsProcessing.TypeVectorPoint]
            )
        )

        self.addParameter(
            QgsProcessingParameterField(
                self.INPUT_MAGASIN_ID_COLUMN,
                self.tr('Magasin id kolonne'),
                None,
                self.INPUT_MAGASIN_POLYGON,
                QgsProcessingParameterField.String
            )
        )
        
        self.addParameter(
            QgsProcessingParameterField(
                self.INPUT_LERTYKKELSE_ID_COLUMN,
                self.tr('Lertykkelses magasin id kolonne'),
                None,
                self.INPUT_LERTYKKELSE_POINT,
                QgsProcessingParameterField.String
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr('Output layer'),
                QgsProcessing.TypeVectorPoint
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        # Retrieve the feature source and sink. The 'dest_id' variable is used
        # to uniquely identify the feature sink, and must be included in the
        # dictionary returned by the processAlgorithm function.
        magasin_source = self.parameterAsSource(
            parameters,
            self.INPUT_MAGASIN_POLYGON,
            context
        )

        # If source was not found, throw an exception to indicate that the algorithm
        # encountered a fatal error. The exception text can be any string, but in this
        # case we use the pre-built invalidSourceError method to return a standard
        # helper text for when a source cannot be evaluated
        if magasin_source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT_MAGASIN_POLYGON))
        
        magasin_table_name = self.parameterAsString(parameters, self.INPUT_MAGASIN_POLYGON, context)
       
        magasin_field_name = self.parameterAsString(parameters, self.INPUT_MAGASIN_ID_COLUMN, context)
        magasin_field = magasin_source.fields().at(magasin_source.fields().lookupField(magasin_field_name))

        lertykkelse_source = self.parameterAsSource(
            parameters,
            self.INPUT_LERTYKKELSE_POINT,
            context
        )
        
        if lertykkelse_source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.lertykkelse_source))

        lertykkelse_table_name = self.parameterAsString(parameters, self.INPUT_LERTYKKELSE_POINT, context)
        
        lertykkelse_field_name = self.parameterAsString(parameters, self.INPUT_LERTYKKELSE_ID_COLUMN, context)
        lertykkelse_field = lertykkelse_source.fields().at(lertykkelse_source.fields().lookupField(lertykkelse_field_name))
        
        output_file = self.parameterAsFileOutput(parameters, self.OUTPUT, context)
             
        saarbarhed_query = f"""
            ?query=
            SELECT {lertykkelse_table_name}.*, {magasin_table_name}.{magasin_field_name} 
            FROM {magasin_table_name} 
            INNER JOIN {lertykkelse_table_name} on {magasin_table_name}.{magasin_field_name}={lertykkelse_table_name}.{lertykkelse_field_name} 
            WHERE st_within({lertykkelse_table_name}.geometry, {magasin_table_name}.geometry)
        """
                        
        feedback.pushInfo(f'Debug SQL: {saarbarhed_query}')

        vLayer = QgsVectorLayer(saarbarhed_query, "temp_saarbarhed", "virtual")
        
        if not vLayer.isValid():
            raise QgsProcessingException(vLayer.dataProvider().error().message())

        if vLayer.wkbType() == QgsWkbTypes.Unknown:
            raise QgsProcessingException(self.tr("Cannot find geometry field"))

        (sink, dest_id) = self.parameterAsSink(parameters, self.OUTPUT, context,
                                               vLayer.fields(), vLayer.wkbType(), vLayer.crs())
        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT))
            
        features = vLayer.getFeatures()
        total = 100.0 / vLayer.featureCount() if vLayer.featureCount() else 0
        for current, inFeat in enumerate(features):
            if feedback.isCanceled():
                break

            sink.addFeature(inFeat, QgsFeatureSink.FastInsert)
            feedback.setProgress(int(current * total))    

        # Return the results of the algorithm. In this case our only result is
        # the feature sink which contains the processed features, but some
        # algorithms may return multiple feature sinks, calculated numeric
        # statistics, etc. These should all be included in the returned
        # dictionary, with keys matching the feature corresponding parameter
        # or output names.
        self.dest_id = dest_id
        
        return {self.OUTPUT: dest_id}
        
    def postProcessAlgorithm(self, context, feedback):
        # https://gis.stackexchange.com/questions/305470/pyqgis-post-processing-of-layer-output-of-qgsprocessingalgorithm
        processed_layer = QgsProcessingUtils.mapLayerFromString(self.dest_id, context)

        # Do smth with the layer, e.g. style it
        symbols = (('Stor sårbarhed', -9999, 5.0, QColor(255, 0, 0)),
                    ('Nogen sårbarhed', 5.0, 15.0, QColor(255, 255, 0)),
                    ('Lille sårbarhed', 15.0, 9999, QColor(0, 255, 0)))

        ranges = []
        for label, lower, upper, color in symbols:
            sym = QgsSymbol.defaultSymbol(processed_layer.geometryType())
            sym.setColor(QColor(color))
            rng = QgsRendererRange(lower, upper, sym, label)
            ranges.append(rng)

        field = "tykkelse"
        renderer = QgsGraduatedSymbolRenderer(field, ranges)
        processed_layer.setRenderer(renderer)
        processed_layer.triggerRepaint()

        #return {self.OUT: self.dest_id}    
        return {}
