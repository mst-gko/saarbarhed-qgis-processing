# Sårbarhedslertykkelse QGIS Processingmodul
QGIS Processingmodul til udledning af magasin-sårbarhed via lertykkelse (trin 1), 
samt sammenligning af lertykkelse med vandtype (trin 2). 

## Installation i QGIS
Kopier Python script saarbarhed_magasin_lertykkelse til C:\Users\XXXXXXX\AppData\Roaming\QGIS\QGIS3\profiles\default\processing\scripts

Udskifte XXXXXXX til dine egne initialer. Hvis mapperne “processing\scripts” ikke findes oprettes de manuelt.

Værktøj er nu klar til brug, og kan findes via QGIS Processing.

![Processing](img/processing_install.png "Installation")


## Trin 1 - Datagrundlag
I [demodata mappen](https://gitlab.com/mst-gko/saarbarhed-qgis-processing/-/tree/main/demodata) findes bland andet to demo tabeller til trin 1: reduceret_lertykkelses_2d.shp og magasiner.shp. De to tabeller skal kunne linkes sammen via et fælles id, som i demo data er mag_alias kolonnen for magasin tabellen og magasin kolonnen fra lertykkelses tabellen.

![Processing](img/link_magasin.png "Data join")

Det oprindelige magasinlag kolonne i magasintabellen brugt et andet magasinnavn end det i lertykkelsestabellen. 
Derfor blev der lavet en ny kolonne kaldet mag_alias. Vær opmærksom på det når I får nyt data.

## Test kørsel på demo data
Åbn demo datasættet i QGIS fra demo mappen. Enten projekt filen _demo_map.qgz eller de to shapefiler.

Start værktøj ved at dobbeltklikke på Sårbarhedsudtræk i Processing Toolbox

Udfyld som her under

![Processing](img/module.png "Module")

Magasin-polygonlag er det lag som har magasinflader
Lertykkelse-punktlag er punkter med magasinlertykkelser

Output layer bliver alle de punkter hvor ‘Magasin id kolonne’ = ‘Lertykkelse magasin id kolonne’ og hvor lertykkelsespunkt ligger indefor magasinpolygon.

Her brugt ved mag_alias kolonnen fra magasin tabellen og magasin kolonnen fra lertykkelsespunkttabellen. Det er vigtigt at man bruger den samme navngivning af magsiner i de to tabeller. I dette tilfælde blev det nødvendigt at lave en ny kolonne mag_alias til magasintabellen, da magasinnavngivning ikke var helt ens mellem de to tabeller.

## Demo result
Kør Processing script og vælg eventuelt et ikke temporært resultat format, hvis resultatet skal gemmes.

![Processing](img/map_theme.png "Map")

![Processing](img/style.png "Style")

## Trin 2 - Sammenlign lertykkelse med vandtypen
Tjekker resultatet fra trin 1 op imod vandtypen fra udvalgte boringer. Det giver et binærtkort (true/false eller grøn/rød om man vil) over vandtype og lertykkelse fra et tilhørende magasin, som er højest 100 meter væk, efter definitionen:

Hvor tabellen output_saarbarhed er resultat fra trin 1 og vandtype_boring er boringer med en vandtype attribut.

```
-- Tilpas id værdier mellem de to tabeller
update temp_jakla.vandtype_boring
set aquifer_si = 'Sand  1' where aquifer_si = 'Sand_1';

update temp_jakla.vandtype_boring
set aquifer_si = 'Sand 2' where aquifer_si = 'Sand_2';

update temp_jakla.vandtype_boring
set aquifer_si = 'Sand 3' where aquifer_si = 'Sand_2';

update temp_jakla.vandtype_boring
set aquifer_si = 'PK' where aquifer_si = 'Kalk';

drop table if exists temp_jakla.ks_wellchem_claythickness_sjaelland;
create table temp_jakla.ks_wellchem_claythickness_sjaelland as
    --Find clay thickness point closest to a reservoir well point with chemistry
    with get_clay_thicknes as (
        select distinct on (kemi.id) ler.id ler_id,
                                     ler.tykkelse ler_claythickness,
                                     ler.magasin ler_magasin,
                                     st_distance(kemi.geom, ler.geom) dist,
                                     kemi.*
        from temp_jakla.output_saarbarhed ler,
             temp_jakla.vandtype_boring kemi
        where st_dwithin(kemi.geom, ler.geom, 100)
          and kemi.aquifer_si = ler.magasin
        order by kemi.id, st_distance(kemi.geom, ler.geom), ler.id
    )
    -- Compute qa boolean from watertype and clay thincness
    select *,
        case
            when vandtype = 'A' and ler_claythickness < 5 then true
            when vandtype = 'B' and ler_claythickness < 5 then true
            when vandtype = 'AB' and ler_claythickness < 5 then true
            when vandtype = 'X' and ler_claythickness < 5 then true
            when vandtype = 'Y' and ler_claythickness between 5 and 15 then true
            when vandtype = 'C1' and ler_claythickness > 5 then true
            when vandtype = 'C2' and ler_claythickness between 5 and 15 then true
            when vandtype = 'D' and ler_claythickness > 15 then true
            else false
        end ks
    from get_clay_thicknes;
```

Kvalitetssikring ved ovenstående SQL giver kortet

![ks](img/ks_vandtype_lertykkelse.png "KS")

## License
GNUv3
